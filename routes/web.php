<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'PostController@index')->name('home');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::get('/post/new', 'PostController@create')->name('post.create');
Route::post('/post/new', 'PostController@store')->name('post.store');
Route::delete('/post/delete/{post_id}', 'PostController@delete')->name('post.delete');
Route::get('/posts/counter', 'PostController@counter')->name('posts.counter');
Route::get('/post/edit/{id}', 'PostController@edit')->name('post.edit');
Route::put('/post/update/{id}', 'PostController@update')->name('post.update');
Route::get('/post/{id}', 'PostController@show')->name('post.show');




Route::post('/add_response/{post_id}', 'ResponseController@store')->name('response.store');
Route::delete('/delete_response/{response_id}', 'ResponseController@delete')->name('response.delete');


Route::get('/profile/{name}', 'ProfileController@show')->name('profile.show');
Route::delete('/profile/delete/{user_id}', 'ProfileController@delete')->name('profile.delete');
Route::get('/profile/edit/{name}', 'ProfileController@edit')->name('profile.edit');
Route::put('/profile/update/{name}', 'ProfileController@update')->name('profile.update');




