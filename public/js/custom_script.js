async function counter() {

    //let token = document.getElementsByTagName("META")[2].content;
    let url = '/posts/counter';
    let response = await fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            //'X-CSRF-Token': token,
        },
        
    })
    let result = await response.json();
    document.getElementById('counter-result').innerHTML = `Number of posts: ${result.posts}`;

};

  
counter();
setInterval(() => counter(), 10000);