@extends('layouts.app')

@section('content')
<section class="profile-page">
<div class="profile d-flex flex-column justify-content-center align-items-center">
<h2>Profile</h2>

    <img class="profile-img" src="/uploads/image/{{ $user->image }}" alt="">
    <p>{{ $user->name }} </p>
    @if(!Auth::guest())
        <p>{{ $user->email }} </p>
    @endif

@if(!Auth::guest() && auth()->user()->name == $user->name)

<a class="btn btn-secondary" href="{{ route('profile.edit', ['name' => auth()->user()->name]) }}"> Edit account</a><br>

<form class="delete" action="{{route('profile.delete', ['user_id' => $user->id])}}" method="POST">
    @csrf     
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">  
    <button type="submit" class="btn btn-secondary">Delete account</button>
</form>

@endif
</div>
<hr style="width:80%">
<div class="current-posts">
    
<h2>Posts ({{ count($userPosts) }})</h2><br>

@foreach ($userPosts as $post)

        <article class="d-flex flex-md-row ">

        <div class="info">

            <small> {{ $post->created_at }}</small>

        </div>

        <div class="content">
            <p> {!! $post->message !!} </p>

            @if(!Auth::guest() && auth()->user()->id == $post->user_id)
            <form class="delete" action="{{route('post.delete', ['post_id' => $post->id])}}" method="POST">
              @csrf
        
            <input type="hidden" name="_method" value="DELETE"> 
            <input type="hidden" name="user_id" value="{{auth()->user()->id}}">       
            <button type="submit" class="btn btn-danger">Delete post</button>
            </form>
            <a class="btn btn-success" href="{{ route('post.edit', ['id' => $post->id]) }}"> Edit post</a>
            @endif
        <br><br><button class="btn btn-warning count-comments">Comments: ({{ count($post->responses) }}) </button><br><br>
        @if($post->responses)

            @foreach ($post->responses as $response)
                <img class="profile-small" src="/uploads/image/{{ $post->user->image }}" alt="">
                <strong>
                <a
                    @if($response->user->id != 1)
                        href="{{ route('profile.show', ['name' => $response->user->name]) }}"
                    @endif
                >

                {{ $response->user->name }}
                </a>
                </strong>
                <small> {{ $response->created_at }}</small><br>
                <p> {{ $response->message}} </p>

                @if(auth()->user() && auth()->user()->id == $response->user_id)
                    <form class="delete" action="{{route('response.delete', ['response_id' => $response->id])}}" method="POST">
                            @csrf
        
                            <input type="hidden" name="_method" value="DELETE"> 
                            <input type="hidden" name="user_id" value="{{auth()->user()->id}}">       
                            <button type="submit" class="btn btn-danger">Delete comment</button><br>
                    </form>
                @endif
                
            @endforeach

        @endif
        @if(auth()->user())
        <form action="{{route('response.store', ['post_id' => $post->id])}}" method="POST">
              @csrf
        
              <label for="message">Add comment</label><br>
              <textarea name="message" id="message" cols="50" rows="2"></textarea>
              <input type="hidden" name="user_id" value="{{auth()->user()->id}}"> <br>      
              <button type="submit" class="btn btn-success">Comment</button>
        </form>
        @endif
        </div>
    </div>
@endforeach
</div>
</div>

@endsection