@extends('layouts.app')

@section('content')

<div class="edit-form" style="background-color: white">
<h2>Edit profile</h2>

<div class="form d-flex justify-content-center align-items-center">

<form method="post" action="{{ route('profile.update', ['name' => $user->name]) }}" enctype="multipart/form-data">
@csrf     
<input type="hidden" name="_method" value="PUT">

<div class="form-group">
<label for="name">Name</label>
{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
<input class="form-control" type="text" id="name" name="name" value="{{ $user->name }}">
</div>

<div class="form-group">
<label for="email">Email</label>
{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
<input class="form-control" type="text" id="email" name="email" value="{{ $user->email }}">
</div>

<div class="form-group">
<label for="pw">Password</label>
{!! $errors->first('password', '<p class="help-block">:message</p>') !!}
<input class="form-control" type="password" id="pw" name="password">
</div><br>

<div class="form-group">
<label for="image">Upload new profile photo</label>
{!! $errors->first('image', '<p class="help-block">:message</p>') !!}
<input style="border:none" class="form-control" type="file" id="image" name="image">
</div><br>

<input type="submit" value="Update"  class="btn btn-primary">

</form>
</div>
</div>
@endsection