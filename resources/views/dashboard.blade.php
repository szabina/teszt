@extends('layouts.app')

@section('content')
<div id="dashboard" class="d-flex flex-md-row flex-column justify-content-around align-items-center">
<div class="cards">

<button type="button" class="dashboard-links" data-toggle="modal" data-target="#exampleModal">
  <h2>Recent posts</h2>
</button>
</div>

<div class="cards">
<h2><a class="dashboard-links" href="{{ route('post.create') }}">New Post</a></h2>
</div>

<div class="cards">
<h2><a class="dashboard-links" href="{{ route('profile.show', ['name' => Auth::user()->name]) }}">Profile</a></h2>
</div>

</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Recent post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @foreach ($posts as $post)

      <div class="modal-body">
                  <img class="profile-small" src="/uploads/image/{{ $post->user->image }}" alt="">

                    <a
                    @if($post->user->id != 1)
                        href="{{ route('profile.show', ['name' => $post->user->name]) }}"
                    @endif>{{ $post->user->name }} 
                    </a>
                    <small>{{ $post->updated_at }} </small>
                    
                     <p>{!! str_limit($post->message, 20) !!}</p>
                     <a href="{{ route('post.show', ['id' => $post->id])}}">Read</a>
      </div>
          
      @endforeach
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
        <button type="button" class="btn btn-secondary"><a href="{{ route('home') }}">Read more posts</a></button>
      </div>
    </div>
  </div>
</div>


@endsection
