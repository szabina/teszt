@extends('layouts.app')

@section('content')
<div class="container form-container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h2>Edit Post</h2></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                
            
                    <form action="{{ route('post.update', ['id' => $post->id]) }}" method="post">
                             @csrf
                        <div class="form-group">
                        <label for="message">Post message:</label><br>
                        {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
                        <textarea class="textarea" name="message" id="message">{!! $post->message !!}</textarea>
        
                        </div><br>
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    </div>
            </div>
        </div>
    </div>
</div>


@endsection
