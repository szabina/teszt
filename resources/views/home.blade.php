@extends('layouts.app')


<script src="{{ asset('js/custom_script.js') }}" ></script>



@section('content')
<br>
<button class="btn btn-secondary" id="counter-result" >Number of posts: </button><br><br>


    @foreach ($posts as $post)
    <article class="d-flex flex-md-row ">

        <div class="info">
        <img class="profile-small" src="/uploads/image/{{ $post->user->image }}" alt="">
        <strong>

        <a
        @if($post->user->id != 1)
         href="{{ route('profile.show', ['name' => $post->user->name]) }}"
         @endif
         >
        
        {{ $post->user->name }}
        </a>
        </strong><br><br>

        <small> {{ $post->updated_at }}</small>

        </div>

        <div class="content">
        <p> {!! $post->message !!} </p><hr>

        @if(auth()->user() && auth()->user()->id == $post->user_id)
            <form class="delete" action="{{route('post.delete', ['post_id' => $post->id])}}" method="POST">
              @csrf
        
              <input type="hidden" name="_method" value="DELETE"> 
              <input type="hidden" name="user_id" value="{{auth()->user()->id}}">       
              <button type="submit" class="btn btn-danger">Delete post</button>
             </form><br><br>


        @endif

        <button class="btn btn-warning count-comments">Comments: ({{ count($post->responses) }}) </button>


        <div class="comments">
        @if(count($post->responses) > 0)
            

            @foreach ($post->responses as $response)
            <img class="profile-small" src="/uploads/image/{{ $response->user->image }}" alt="">

                <strong>
                <a
                    @if($response->user->id != 1)
                        href="{{ route('profile.show', ['name' => $response->user->name]) }}"
                    @endif
                >

                {{ $response->user->name }}
                </a>
                </strong>
                <small> {{ $response->created_at }}</small><br>
                <p> {{ $response->message}} </p>
                @if(auth()->user() && auth()->user()->id == $response->user_id)
                    <form class="delete" action="{{route('response.delete', ['response_id' => $response->id])}}" method="POST">
                            @csrf
        
                            <input type="hidden" name="_method" value="DELETE"> 
                            <input type="hidden" name="user_id" value="{{auth()->user()->id}}">       
                            <button type="submit" class="btn btn-danger">Delete comment</button><br>
                    </form>
                @endif
                
            @endforeach

        @endif 

        @if(auth()->user())
        <form action="{{route('response.store', ['post_id' => $post->id])}}" method="POST">
              @csrf
        
              <label for="message">Add comment:</label><br>
              {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
              <textarea name="message" id="message" cols="50" rows="2"></textarea><br>
              <input type="hidden" name="user_id" value="{{auth()->user()->id}}"> <br>      
              <button type="submit" class="btn btn-success">Comment</button>
        </form>
        @endif

          </div>
        </div>
    </article>
    @endforeach
    {{ $posts->links() }}

@endsection