<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Post;
use App\Models\Response;
use Illuminate\Support\Facades\Auth;


class ResponseController extends Controller
{
    public function store(Request $req, string $postId) {
        
        $this->validate($req, [
            'message' => 'required'
          ]);

        $response = new Response;
        $response->message = $req->input('message');
        $response->user()->associate($req->input('user_id'));
        $response->post()->associate($postId);
        $response->save();

        return redirect()->back();
    }
    
    public function delete(Request $req, string $responseId){
        $response = Response::find($responseId);
        if($req->input('user_id') == $response->user_id){
            $response->delete();
        }
        return redirect()->back();
    }
}
