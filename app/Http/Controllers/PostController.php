<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    public function create(){
        if(auth()->user()){
            return view('post.create');
        }
        return redirect()->route('home');
    }

    public function store(Request $req){
        $this->validate($req, [
            'message' => 'required'
          ]);

        $post = new Post;
        $post->message = $req->input('message');
        $post->user()->associate($req->input('user_id'));
        $post->save();

        return redirect()->route('home');
    }

    public function show(string $id) {
        $post = Post::find($id);
        return view('post.show')->with('post', $post);
    }

    public function edit(string $id){
        $post = Post::find($id);
        if(auth()->user()->id == $post->user_id){
        return view('post.edit')->with('post', $post);
        }
        return redirect()->back();
    }

    public function update(Request $req, string $id){
        $this->validate($req, [
            'message' => 'required'
          ]);

        $post = Post::find($id);
        if(auth()->user()->id == $post->user_id){
            $post->message = $req->input('message');
            $post->save();
            return redirect()->route('home');
        }
        return redirect()->back();
    }

    public function index() {
        $posts = Post::orderBy('updated_at', 'desc')->paginate(5);
        return view('home')->with('posts', $posts);
    }

    public function delete(Request $req, string $postId){
        $post = Post::find($postId);
        if($req->input('user_id') == $post->user_id){
            $post->delete();
        }
        return redirect()->back();
    }
    public function counter()
    {
        $data = Post::count(); 
            
            return response()->json([
                'posts'  => $data,
            ], 200);
        

        //return redirect()->back();
            
    }
}
