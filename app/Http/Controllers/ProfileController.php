<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Post;
use App\Models\Response;
use Image;

use Illuminate\Support\Facades\Auth;


class ProfileController extends Controller
{
    public function show(String $name){

        $user = User::where('name', $name)->first();
        $userPosts = Post::where('user_id', $user->id)->get();
        return view('profile.show')->with('user', $user)->with('userPosts', $userPosts);
        

    }

    public function delete(Request $req){
        $user_id = $req->input('user_id');
        if(auth()->user()->id == $user_id){
            $user = User::find($user_id);
            $userPosts = Post::where('user_id', $user->id)->get();
            foreach ($userPosts as $post) {
                $post->user()->dissociate($user_id);
                $post->user()->associate(User::cancelled_id);
                $post->save();
            }
            $userResponses = Response::where('user_id', $user_id)->get();
            foreach ($userResponses as $response) {
                $response->user()->dissociate($user_id);
                $response->user()->associate(User::cancelled_id);
                $response->save();
            }
            $user->delete();
            return redirect()->route('home');
        }
        return redirect()->back();
    }

    public function edit(string $name) {
        if(auth()->user()->name === $name) {
            $user = User::where('name', $name)->first();
            return view('profile.edit')->with('user', $user);
        }
        return redirect()->back();
    }

    public function update(Request $req, string $name){
    
        if(auth()->user()->name == $name) {
        
        $user = User::where('name', $name)->first();
        $id = $user->id;

        $this->validate($req, [
            'name' => 'required|unique:users,name,' .$id,
            'email' => 'required|email|unique:users,email,' .$id,
            'password' => 'required|min:6',
            'image' => 'image'
          ]);

          if($req->hasFile('image')){
              $image = $req->file('image');
              $filename = time() . '.' . $image->getClientOriginalExtension();
              Image::make($image)->resize(300, 300)->save( public_path('/uploads/image/' . $filename));
              $user->image = $filename;
          }

          $user->name = $req->input('name');
          $user->email = $req->input('email');
          $user->password = \Hash::make($req->input('password'));
          $user->save();

          return redirect()->route('home');
        }
        
        return redirect()->back();
    }
}
