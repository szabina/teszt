<?php

use Illuminate\Database\Seeder;
use App\Models\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post1 = new Post;
        $post1->message = "Ez az első bejegyzés";
        $post1->user_id = 2;
        $post1->save();

        $post2 = new Post;
        $post2->message = "Ez a második bejegyzés";
        $post2->user_id = 2;
        $post2->save();

        $post3 = new Post;
        $post3->message = "Ez a harmadik bejegyzés";
        $post3->user_id = 2;
        $post3->save();

        $post4 = new Post;
        $post4->message = "Ez a negyedik bejegyzés";
        $post4->user_id = 2;
        $post4->save();

        $post5 = new Post;
        $post5->message = "Ez az ötödik bejegyzés";
        $post5->user_id = 2;
        $post5->save();

        $post6 = new Post;
        $post6->message = "Ez a hatodik bejegyzés";
        $post6->user_id = 2;
        $post6->save();

    }
}
