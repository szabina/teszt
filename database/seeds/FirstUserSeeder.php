<?php

use Illuminate\Database\Seeder;
use App\User;


class FirstUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = "Test user";
        $user->email = "testuser@test.hu";
        $user->password = \Hash::make("testuser");
        $user->save();
    }
}
