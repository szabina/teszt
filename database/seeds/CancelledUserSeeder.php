<?php

use Illuminate\Database\Seeder;
use App\User;

class CancelledUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cancelled = new User;
        $cancelled->name = "Cancelled user";
        $cancelled->email = "Cancelled user";
        $cancelled->password = \Hash::make("Cancelled user");
        $cancelled->id = User::cancelled_id;
        $cancelled->save();
    }
}
