<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CancelledUserSeeder::class);
         $this->call(FirstUserSeeder::class);
         $this->call(PostSeeder::class);


    }
}
